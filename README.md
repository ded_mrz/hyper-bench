# hyper-bench #

This tool is a part of my submission to Free TON contest "#3 RTDB: Hypercore test-drive".

https://forum.freeton.org/t/prolongation-of-the-rtdb-hypercore-test-drive-contest/5287/4?u=creator

### Installation ###

```
git clone https://bitbucket.org/ded_mrz/hyper-bench
cd hyper-bench
npm install
```

Then you can run benchmarks.
```
node benchmark.js --N=1000000 --id=a --run_each=5 --ram
```
