const seed_random = require('seed-random')

module.exports = {
    setSeed: seed => SEED = seed,
    dsGenerator,
    keyGenerator,
    coreIdxGenerator,
};

let SEED = '';

function dsGenerator({size=1e9, min,max, seq='seq' , fill=Buffer.from([0])}) {
    if (!max) max = min;
   const randomly = seq == 'random';
   let step = (max-min)/  (randomly ?1 :size);
   if (seq == 'back') {
       [min, max] =[max, min];
       step=-step;
   }
   const args = arguments;
   return function*() {
       const seed = SEED+JSON.stringify({size,min,max,seq,fill});
       console.log('generator seed',seed);
       const random = seed_random(seed);
      for (let i = 0; i < size; i++) {
         const multiplier = randomly ? random() : i;
         const sz        = Math.round(min + multiplier * step);
         yield Buffer.alloc(sz, fill);
      }
   }
}

function coreIdxGenerator({size, min, max, step=null, seq='seq', txt=false, parts=1, sep='/',      t,set}) {
    if (t) return keyGenerator({size, min, max, seq, set, parts, sep});
    // sequence is looped if read after size
    if (!max) max = min;
        const randomly = seq == 'random';
        if (randomly) step = null;
        if (!size) size =max-min;
        if (step == null){
            step = (max-min)/  (randomly ?1 :size);
        }
        if (seq == 'back') {
            [min, max] =[max, min];
            step=-step;
        }
        return function*() {
            const seed = JSON.stringify({size, min, max, step, seq, txt, parts, sep});
            const random = seed_random(SEED +seed);
           for (let i = 0; ; i++) {
                const multiplier = randomly ? random() : (i % size);
                let idx= Math.round(min + multiplier * step);
                if(txt)
                {
                    const b = {a:36,h:16}[txt] || 10
                   maxlen = max.toString(b).length;
                   if (maxlen < parts) maxlen = parts;
                //    idx = ''+idx;
                   idx = idx.toString(b)
                   while (idx.length < maxlen) idx = '0' + idx;
                   if( parts > 1)
                   {
                     let part = maxlen / parts;
                     let result = idx.substring(0, Math.floor(part));
                     for ( let j=1; j<parts; j++ )
                     {
                         let a = Math.floor(j*part)
                         let b = Math.floor((j+1)*part)
                         result = result + sep + idx.substring(a,b);
                     }
                     idx = result;
                   }
                }
               yield idx
           }
        }
}

function keyGenerator({size=1e6, min, max, seq='seq', set='aA#!', parts=1, sep='/'})
{
   if (!max) max = min;
    const randomly = seq == 'random';
    let step = (max-min)/  (randomly ?1 :size);;
    if (seq == 'back') {
        [min, max] =[max, min];
        step=-step;
    }
    const mask = charsMask(set)
    return function*() {
        const seed = JSON.stringify({size, min, max, seq, parts, set,sep});
        const random = seed_random(SEED +seed);
       for (let i = 0; i < size; i++) {
           const key = [];
           for (let part = 0; part < parts; part++) {
               const multiplier = randomly ? random() : i;
               const sz         = Math.round(min + multiplier * step);
               key.push(randomString(sz, mask, random));
           }
           yield key.join(sep)
       }
    }
    return keys;
}

function charsMask(chars) {
    var mask = '';
    if (chars.includes('a')) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.includes('A')) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.includes('#')) mask += '0123456789';
    if (chars.includes('!')) mask +='~!#$%()_+-=;,.';
    return mask;
}

function randomStringFromSet(length, chars='aA#!') {
    var mask = charsMask(chars);
    return randomString(length, mask);
}

function randomString(length, mask, randomFn = random) {
    var result = '';
    var  len = mask.length
    while (length-- > 0) result += mask[Math.floor(randomFn() * len)];
    return result;
}