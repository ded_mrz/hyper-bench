const hypercore = require('@geut/hypercore-promise')
const hyperdrive = require('@geut/hyperdrive-promise')
const hypertrie = require('../hypertrie-promise')
const hyperbee = require('hyperbee');
const ramStorage = require('random-access-memory')

let O = {};

module.exports = {
    setOptions: o => O = o,

    core: () => new mycore(),
    drive: () => new mydrive(),
    trie: () => new mytree(), // todo: with given hypercore
    bee: () => new mybee(),
};

class mycore {
    init(name, overwrite=true) {
        // _db = hypercore('./my-first-dataset', {valueEncoding: 'utf-8'})
        this._db = hypercore(O.RAM ? ramStorage : `${O.STORE}/${name}`, {overwrite})
        this._db.on('error', console.error);
    }
    // _db = hypercore();
    close() { return this._db.close();}
    stats() {
        return {len: this._db.length, bytes:this._db.byteLength}
    }

    put(k,v,i){
        //ignore key here
        if (i && i %1e6 === 0) {
            // console.info(i, this._db.stats)
            console.info(i, this._db.length, this._db.byteLength)
        }
        return this._db.append(v);
    }

    get(k) {
        return this._db.get(k);
    }
}

class mydrive {
    init(name, overwrite=true) {
        this._db = hyperdrive(O.RAM ? ramStorage : `${O.STORE}/drive-${name}`)
        this._db.on('error', console.error);
        this._l = 0;
        this._b = 0;
    }
    // _db = hypercore();
    close() { return this._db.close();}
    kill() { return this._db.destroyStorage();}
    stats() {
        return {len: this._l, bytes:this._b}
    }

    put(k,v,i){
        if (i&&i %1e6 === 0) {
            console.info(i, this._l, this._b)
        }
        this._l++;
        this._b += (""+k).length + v.length;
        return this._db.writeFile(''+k, v);
    }

    get(k) {
        return this._db.readFile(''+k);
    }
    del(k) {
        return this._db.unlink(k)
    }
}

class mytree {
    init(name, overwrite=true) {
        this._db = hypertrie(O.RAM ? ramStorage : `${O.STORE}/trie-${name}`, {overwrite})
        this._db.on('error', console.error);
        this._l = 0;
        this._b = 0;
    }
    close() { return ;}//this._db.close();}
    // kill() { return this._db.destroyStorage();}
    stats() {
        return {len: this._l, bytes:this._b}
    }

    put(k,v,i){
        if (i&&i %1e6 === 0) {
            console.info(i, this._l, this._b)
        }
        this._l++;
        this._b += k.length + v.length;
        return this._db.put(k, v);
    }

    get(k) {return this._db.get(k); }
    del(k) {return this._db.del(k); }
}

class mybee {
    init(name, overwrite=true) {
        this._feed = hypercore(O.RAM ? ramStorage : `${O.STORE}/bee-${name}`, {overwrite})
        this._db = new hyperbee(this._feed)
        this._feed.on('error', console.error);
        this._l = 0;
        this._b = 0;
        return this._db.ready()
    }
    close() { return this._feed.close();}
    kill() { return this._feed.destroyStorage();}
    stats() {
        return {len: this._l, bytes:this._b}
    }

    put(k,v,i){
        if (i&&i %1e6 === 0) {
            console.info(i, this._l, this._b)
        }
        this._l++;
        this._b += k.length + v.length;
        return this._db.put(k, v);  
    }

      get(k) {return this._db.get(k);}
    del(k) {return this._db.del(k); }    }