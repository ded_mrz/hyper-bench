const fs  = require('fs')
const seed_random = require('seed-random')
const args = require('minimist')(process.argv.slice(2))

const STORE = './tmp_store'; // direcrory to store feed data
const RESFILE = args.o || './results.txt'; // file to store results
const SEED = args.seed || 'Free TON';
console.log('Seed:',SEED);
const RAM = args.ram
const RUNS = parseInt(args.runs) || 5; //disabled
const RUN_EACH = parseInt(args.run_each) || 1;
if (RUNS < 0) throw "Runs can't be negative";
if (RUN_EACH < 0) throw "Run each can't be negative";
console.log('ram',RAM);
const ID=args.id;

const G = require('./tools/generators')
G.setSeed(SEED);

const dbs = require('./tools/dbs');
const { exit } = require('process');
dbs.setOptions({RAM, STORE});


const random = seed_random(SEED);

const N = parseInt(args.N) || 1e5;
if (N < 100) throw "N can't be less than 100"


const plans = [
    // 'core,trie,bee,drive, sonar'
    // [type:R/W, dataset, size, db, keyset, reads , readkeyset]
    ['W', 'ds256', N, 'trie', ]
]

function makeWPlanKeys(db, which, ds='ds256') {
    const ks1 = ["int",
"b1k","b10k","b1m","r1k","r10k","r1m","t0seq","tAseq",
]
    const ks2 = [
"r0micro","rZmicro","rlong","rlonger","rlongest",
"r16d","r64d","r256d","r1kd",
"seq2da","seq2d0","seq3da","seq3d0",    ]
        let ks = [];
        if (which === 1 || which === 3) {
            ks = [...ks1];
        }
        if (which >= 2) {
            ks = [...ks, ...ks2]
        }
    return ks.map(k => ['W', ds, N, db, k]);
}

function makeWPlanMaxItems(db='core', ds='ds32') {
    const plans =[];
    for(var i=-5; i<=10;i++){
       plans.push(['W', ds, Math.floor(N*2**i)||1, db])
    }
    return plans
}

function makeRPlanMaxItems(db='core', ds='ds32') {
    return makeWPlanMaxItems(db, 'R', ds);
}
function makeWPlanMaxItems(db='core', rw='W', ds='ds32') {
    const plans =[];
    for(var i=-5; i<=10;i++){
       plans.push([rw, ds, Math.floor(N*2**i)||1, db])
    }
    return plans
}

function makeWPlan(db, rw='W') {
    const ds = ['ds32', 'ds64', 'ds128', 'ds256', 'ds512', 'ds1k', 'ds4k', 'ds16k', 'ds64k', 'ds256k', 'ds1m', 'ln1_1m', 'ln1m_1', 'r1_1m', 'r16k_1m', 'r32_1k', 'r1k_64k', 'r4k_256k', ]
    const sizes = {r1_1m:N/4,
        r16k_1m:N/4, ds1m:N/4};//..
    return ds.map(x => [rw, x, sizes[x]||N, db]);
}

function makeWPlanLarge() {
    const ds = ['ds256k', 'ds1m', 'ds4m', 'ds8m', ]
    return ds.map(x => ['W', x, N, 'core']);
}

function makeRPlanSimple(db) {
    return makeWPlan(db, 'R');
}

function makeRPlan(db, reads) {
    const ds = ['ds32','ds128','ds1k', 'ds16k', 'ds256k', 'ds1m']
    const ks = ['b1k','b10k','r10k']
    let plans = [];
    for (const d of ds) for (const k of ks) plans.push(['R',d,N,db,'int',reads,k])
    return plans;
}

function makeRPlanKeys(db, reads, ds ='ds256') {
    const ks = ["r0micro","rZmicro","rlong","rlonger","rlongest",
    "r16d","r64d","r256d","r1kd",
    "seq2da","seq2d0","seq3da","seq3d0",    ]
    return ks.map(k => ['R',ds,N,db,k,reads,k])
}

const ds = {
    ds32: {min: 32},
    ds64: {min: 64},
    ds128: {min: 128},
    ds256: {min: 256},
    ds512: {min: 512},
    ds1k: {min: 1024},
    ds4k: {min: 4096},
    ds16k: {min: 16384},
    ds64k: {min: 65536},
    ds256k: {min: 2**18},
    ds1m: {min: 2**20},
    ds4m: {min: 2**22},
    ds8m: {min: 2**23},
    'ds8m+': {min: 2**23+1},
    ln1_1m: {size:N/4, min: 1, max:2**20},
    ln1m_1: {size:N/4, min: 1, max:2**20, seq:'back'},
    r1_1m: {min: 1, max:2**20, seq:'random'},
    r16k_1m: {min: 16384, max:2**20, seq:'random'},
    r32_1k: {min: 32, max:1024, seq:'random'},
    r1k_64k: {min: 1024, max:65536, seq:'random'},
    r4k_256k: {min: 4096, max:65536*4, seq:'random'},
};

const ks = {
    int: {min:0,max:1e10, step :1},
    b1k: {min:0,max:1e3,step:1, seq:'back'},
    b10k: {min:0,max:1e4,step:1, seq:'back'},
    b1m: {min:0,max:1e6,step:1, seq:'back'},
    r1k: {min:0,max:1e3,seq:'random'},
    r10k:{min:0,max:1e4,seq:'random'},
    r1m: {min:0,max:1e6, seq:'random'},

   t0seq: {min:0,max:1e6, txt:"0"},
   tAseq: {min:0,max:1e6, txt:"a"},
   tHseq: {min:0,max:1e6, txt:"h"},
   r0micro: {min:0,max:9, txt:"0", seq:"random"},
   rAmicro: {min:0,max:35, txt:"a", seq:"random"},
   rZmicro: {t:1, min:1, seq:"random"},
   rlong: {min:0, max: 2**63, txt:"a", seq:"random"},
   rlonger: {t:1, min:256, seq:"random"},
   rlongest: {t:1, min:4096, seq:"random"},

   r16d: {t:1, min:4, parts:16, seq:"random"},
   r64d: {t:1, min:3, parts:64, seq:"random"},
   r256d: {t:1, min:2, parts:256, seq:"random"},
  r1kd: {t:1, min:1, parts:1024, seq:"random"},


  seq2da: {min:0, max: 1e6, size:10000, parts:2, txt:'a'},
 seq3da: {min:0, parts:3, max:1e6, size:10000,  txt:'a'},
 seq2d0: {min:0, max: 1e6, size:10000, parts:2, txt:'0'},
 seq3d0: {min:0, parts:3, max:1e6, size:10000,  txt:'0'},
 seq2dh: {min:0, max: 1e6, size:10000, parts:2, txt:'h'},
 seq3dh: {min:0, parts:3, max:1e6, size:10000,  txt:'h'},
}


const promiseTimeout = function(ms, promise,info){

    // Create a promise that rejects in <ms> milliseconds
    let timeout = new Promise((resolve, reject) => {
      let id = setTimeout(() => {
        clearTimeout(id);
        reject('Timed out in '+ ms + 'ms.', info)
      }, ms)
    })
  
    // Returns a race between our timeout and the passed in promise
    return Promise.race([
      promise,
      timeout
    ])
  }






let timer =
{
    start: () => {
        const now = Date.now();
        if (!this.first) this.first = now;
        this.s = now;
    },
    end: () => {
        const now = Date.now();
        this.last = now - this.first;
        return now - this.s;
    },
    total:() => this.last-this.first,
};

async function doBench(bench, plan) {
    try {
        // timer.start();
        let result = await bench(plan, timer);
        // let time = timer.end();
        const time = result.t;
        const ts = val => Math.round(val*1e6/time)/1e3;
        console.log(plan, 'DONE', time, (time/1e3)|0, result, `${ts(result.len)}chunks/s`, `${ts(result.bytes)}bytes/s`)
        saveStats(`${"DR"[RAM|0]},${plan},${time},${result.size},${result.len},${result.bytes},${ts(result.len)},${ts(result.bytes)}\n`)
    } catch(e) {
        console.error('failed', e);
    }
}

async function doPlan(plan, timer) {
    [type, dsGen, size,databases,ksGen='int', reads, readGen] = plan;
    const gen = G.dsGenerator( ds [dsGen]);
    const keyset = ks [ksGen]
    const kgen = G.coreIdxGenerator(keyset)();
    const dbList =  databases.split(',');
    for (let dbName of dbList){
        const db = dbs[dbName]();
        if(type== 'W' || type == 'R') {
          await  db.init( `${dbName}-${dsGen}`);
           let i = 0;
           let runs = 1;
           if (type == 'W')          {timer.start(); runs = RUN_EACH;}
           else {console.log('plase wait. creating dataset...')}
           for( let chunk of gen() ) {
                const k = kgen.next().value;
            //    await promiseTimeout(10000, db.put(k, chunk), i) ;

               await db.put(''+k, chunk, i);
               if (++i >= size) break;
           }
           if (!reads) reads = i;
           var t;
           if (type == 'W')t=timer.end();
           if (type=='R') {
            console.log('benchmark read preformance');
            const rgen = G.coreIdxGenerator((readGen && ks[readGen])||ks[ksGen])() // use the same generator if not set
             let i = 0;
             timer.start();
            for ( let k of rgen) {
                if (++i >= reads) break;
                let x = await db.get(k)
            }
             t=timer.end();
           }
           var s = db.stats();
           await db.close()
           if  (db.kill) await db.kill()
           return {t, size, ...s}

        } else{
            throw "Plan type Not implemented"
        }
    }
 }




function saveStats(st) {
    // const str = (new Date()).toISOString().slice(0, 19).replace(/-/g, "/").replace("T", " ");
    const str = (new Date()).toISOString().slice(0, 19).replace("T", " ");
    fs.appendFileSync(RESFILE, `${str},${st}`, { valueEncoding: 'utf8' })
  }


console.log(random(), random(), random(), random(), random());
console.log('--------------------------------')

async function run(plans) {
    for (let pl of plans)
    {
       for(let i = 0; i < RUN_EACH; i++)
         await doBench(doPlan, pl);
    }
}

const predefined = {
    a: () => run(makeWPlan('core')),
    a1: () => run(makeWPlanLarge()),
    ae: () => run([['W', 'ds8m+', 10, 'core']]),
    ba: () => run(makeWPlan('bee')),
    bb: () => run(makeWPlan('trie')),
    bc: () => run(makeWPlan('drive')),
    c: () => {run(makeWPlan('core'));run(makeWPlan('core'))},
    d: () => run(makeWPlanMaxItems()),
    ea: () => run(makeWPlanMaxItems('bee')),
    eb: () => run(makeWPlanMaxItems('trie')),
    ec: () => run(makeWPlanMaxItems('drive')),
    fa1: () => run(makeWPlanKeys('bee',1)),
    fa2: () => run(makeWPlanKeys('bee',2)),
    fb1: () => run(makeWPlanKeys('trie',1)),
    fb2: () => run(makeWPlanKeys('trie',2)),
    fc1: () => run(makeWPlanKeys('drive',1)),
    fc2: () => run(makeWPlanKeys('drive',2)),

    r: () => run(makeRPlanSimple('core')),
    ra: () => run(makeRPlanSimple('bee')),
    rb: () => run(makeRPlanSimple('trie')),
    rc: () => run(makeRPlanSimple('drive')),

    s: () => run(makeRPlan('core',N)),
    sa: () => run(makeRPlan('bee',N)),
    sb: () => run(makeRPlan('trie', N)),
    sc: () => run(makeRPlan('drive', N)),

    ta: () => run(makeRPlanKeys('bee',N)),
    tb: () => run(makeRPlanKeys('trie',N)),
    tc: () => run(makeRPlanKeys('drive',N)),

    q: () => run(makeRPlanMaxItems('core' )),
    qa: () => run(makeRPlanMaxItems('bee')),
    qb: () => run(makeRPlanMaxItems('trie')),
    qc: () => run(makeRPlanMaxItems('drive')),
}

const benchmark = predefined[ID];
if (benchmark) benchmark();
else console.error( 'Unknown predefined benchmark id', ID);
